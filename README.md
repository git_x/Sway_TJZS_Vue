# Sway_TJZS_Vue

#### 项目介绍
2018-12-01  天津智识教育科技有限公司  Sway商战大赛项目，前端开发。


#### 软件架构
软件架构说明
此项目为vue项目，依赖许多中间件。


#### 安装教程

1. npm install
2. npm run dev

#### 使用说明

> A vue project for TCU . maded by yexuan...

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

#### 参与贡献

1. yexuan (eric)
2. lilianxin (ppx)
3. guapi
4. 天津智识教育科技
